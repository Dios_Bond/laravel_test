<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mashine;
use App\Models\BackupDay;
use App\Models\BackupFiles;
//use app\Http\Requests\MashineCreateRequest;

class MashineController extends Controller
{
    public function createmashine(Request $req){
        $mashine = new Mashine;
        $mashine->name = $req->input('name');

        $mashine->save();

        //return redirect()->route('/');
    }
    //
    public function getallmashine(){
        //return "ok";
        $mashine = new Mashine;
        dd($mashine->all());

    }

    public function getbackupday(Request $req){
        $backupday = new BackupDay;
        $mashine_id = $req->input('mashine_id');
        $date = BackupDay::where('name', $mashine_id)->value('date');
        dd($date);
    }

    public function getbackupfile(Request $req){
        $backupfile = new BackupFiles;
        $date = $req->input('date');
        $mashine_id = $req->input('mashine_id');
        $files = $backupfile->where(['mashine_id', $mashine_id], ['date', $date]);
        dd($files);
    }
}
