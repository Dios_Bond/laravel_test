<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/login', function () {
//    return view('login');
//})->name('login');

//Route::post('/data/getmashine', function () {
//    return Request::all();
//});

Route::post('/data/getmashine', function () {
//Route::post('/data/getmashine', 'UserController@check');
// -> name('contact-form');
// {
    return Request::all();
});

Route::post('/data/getdatabymashine', function () {
    return Request::all();
});

Route::post('/dddata/getdatabymashine', function () {
    dd(Request::all());
});

Route::get('/gett', function () {
    return "OK";
});

Route::get('/form', function () {
    return view("post_form");
});

//Route::post('/form', function () {
Route::post('/new/mashine', 'MashineController@createmashine')->name('newmashine');
    // -> name('contact-form');
    // {
//        return Request::all();
//});
Route::get('/getdata/mashine', 'MashineController@getallmashine')->name('getmashine');
Route::get('/getdata/backupday', 'MashineController@getbackupday')->name('getbackupday');
Route::get('/getdata/backupfile', 'MashineController@getbackupfile')->name('getbackupfile');

